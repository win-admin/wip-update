# WIN Work in Progress (WIP) Meetings - Update Proposal 2021


------

<mark>This repository is no longer in use. The update exercise has been completed and all approved changes introduced. All material relevant to WIPs is now hosted on [https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs)</mark>

------


**Outline**

* [Context](#context)
* [Problem 1: Research theme representation](#problem-1-research-theme-representation)
  * [Solution: Promoting the value of translational and clinical insight](#solution-promoting-the-value-of-translational-and-clinical-insight)
  * [Solution: Local theme promotors](#solution-local-theme-promotors)
  * [Solution: WIP triggers](#solution-wip-triggers)
  * [Solution: Extended content](#solution-extended-content)
* [Problem 2: Booking administration and process knowledge](#problem-2-booking-administration-and-process-knowledge)
  * [Solution: Calpendo for WIP and WIN Wednesday booking](#solution-calpendo-for-wip-and-win-wednesday-booking)
  * [Solution: Intranet WIP guide](#solution-intranet-wip-guide)
* [What are we doing?](#what-are-we-doing)
* [How can you contribute?](#how-can-you-contribute)
* [Contact](#contact)

## Context
The Work in Progress (WIP) meetings are scheduled as part of the WIN Wednesday meetings that take place on Wednesdays 12:00 - 13:00. Everyone who is affiliated with the WIN is welcome to attend these meetings.

WIPs provide an informal and constructive forum for researchers to get feedback on planned research projects from a diverse WIN audience. In some cases, WIP presentations are a mandatory stage in gaining the appropriate approvals to conduct new data collection using WIN facilities.

## Problem 1: Research theme representation
WIN research can be broadly divided into four core themes:

- Analysis Research
- Basic Neuroscience
- Clinical Neuroscience
- Physics Research

An analysis of WIP presenters in 2019 (taken as an indicative pre-pandemic year), shows that most presentations are lead by researchers associated with Basic or Clinical Neuroscience, one from a Physics group, and non from the Analysis groups. One presentation was also given by a research who's primary affiliation was in the Department of Experimental Psychology (see Figure 1).

![WIP 2019 presenters by theme](WIPS-2019-byTheme.png)
*Figure 1: Research themes of WIP presenters in 2019*

The impact of this uneven research theme distribution is multifactoral:

1. WIN members do not have an accessible opportunity to learn about the full breadth of research being conducted at WIN.
2. Physics and Analysis researchers do not have an open forum for feedback from applied researchers regarding the translational and clinical aspects of their work.
3. Physics and Analysis students do not have an equal opportunity to present their research to a broad audience.

The proposed changes to the practice and scope of WIP meetings seeks to address this uneven distribution of WIP presentations to: 1) more completely communicate the breadth of WIN research; 2) highlight and encourage researchers to consider the translational value of their work across the centre; 3) give equal opportunity to WIN students and Early Career Researchers to present to a broad and receptive audience.

### Solution: Promoting the value of translational and clinical insight

WIN is committed to delivering research with a high potential for translational impact. As such we aim to foster close relationships between researchers working across all stages of methods development and clinical application. Such relationships and knowledge of each others activities can help inform research design and positioning within a wider clinical context.

We would like to promote the value of a two-way conversation between methods development and applications researchers, so each can contribute their unique expertise at the early stages of a research project. Such conversations happen in the direction of methods-to-applied in the current WIP meetings, but we would like to encourage methods researchers to invite contributions from applied researchers to gaining a broader understanding of the translational context and impact of their work early on.

### Solution: Local theme promotors

The Core WIN individuals named below have agreed to promote WIP participation in their respective themes, in alignment with the ethos of informal and constructive interdisciplinary feedback with a specific focus on promoting conversations around translation.  

- Analysis: taylor.hanayik@ndcn.ox.ac.uk
- Physics: mohamed.tachrount@ndcn.ox.ac.uk or aaron.hess@ndcn.ox.ac.uk
- Preclinical: claire.bratley@ndcn.ox.ac.uk
- Clinical OHBA: clare.odonoghue@psych.ox.ac.uk
- Clinical FMRIB: jessica.walsh@ndcn.ox.ac.uk
- Clinical Neurosciences: marieke.martens@psych.ox.ac.uk
- Cognitive Neuroscience (MRI): sebastian.rieger@psych.ox.ac.uk
- Cognitive Neuroscience (MEG/EEG): anna.camera@psych.ox.ac.uk
- All other themes or questions: cassandra.gouldvanpraag@psych.ox.ac.uk

### Solution: WIP triggers

WIP presentations are required before ethical approval is granted on new data collection projects undertaken using WIN MRI facilities. There is no such mandatory "trigger" to initiate a WIP presentation for teams who collect data using other systems (for example MEG or EEG) or do not require ethical approval. This includes work undertake on existing project codes (for example "physics development") or secondary data analysis. Identifying an appropriate opportunity to present a WIP for such projects may normalise the engagement of currently under-represented research themes and create opportunities for two-way conversation as described above.

To intentionally maximise the training and development benefit of WIP presentation, a new trigger could be implemented in alignment with doctoral student progression, for example at the student transfer (third term of year 1) or confirmation (third term of year 3). We are keen to discuss the most appropriate point in student progression with these researchers, noting that WIPs should take place once a project idea is well formulated but sufficiently early to incorporate feedback.

We should also investigate opportunities to present WIPs for research lead by postdoctoral researchers or using other facilities where an MRI project code is not required.

### Solution: Extended content

WIN recognises that considerable research effort and expertise goes into projects which do not involve the collection of new experimental or observational data. We would like to promote knowledge exchange around such projects by broadening the range of activities which are deemed appropriate for WIP presentations. This might include projects involving:
- secondary data analysis;
- new software applications or extensions;
- training materials;
- experimental protocols;
- public engagement activities;

## Problem 2: Booking administration and process knowledge

When a researcher wishes to present a WIP, they rely on word-of-mouth regarding the process for arranging the meeting. Such undocumented processes contribute to poor inclusivity through the potential for unequal access to knowledge.

The current process for booking a WIP entails several email exchanges between researchers, the WIP lead, and WIN Administrators, to identify available slots and receive appropriate documentation and notices. This is an inefficient use of resources, and delays in communication can become problematic when there is high demand for presentation slots.

The meeting slots available for WIP presentations are also shared with other WIN Wednesday content (such as training and [Equality, Diversity and Inclusivity](https://www.win.ox.ac.uk/about/edi) presentations). The Coordinators of WIN Wednesday content would also benefit from an efficient solution for booking their meetings against a shared calendar and collating appropriate documentation.  

### Solution: Calpendo for WIP and WIN Wednesday booking

[Calpendo](https://calpendo.fmrib.ox.ac.uk/bookings/) is used throughout WIN to book research facilities and meeting spaces. We propose that researchers use calpendo to directly book their WIP slot, to improve efficiency and reduce the administrative burden in arranging presenters.

Calpendo can also be used to collate the required documentation and set deadlines for required documentation. For example, it may be beneficial to make it mandatory to upload some information (for example project title and abstract) at the point of booking. The file [booking-proces.md](booking-process.md) is being used to track the current logistics and communications around booking, and will be used to create a specification for the calpendo facility.

If required, calpendo may also make it easier to produce reports on historical WIP activity, to review the efficacy of any initiatives to improve uptake.

WIN Wednesday coordinators have also agreed to use calpendo for booking other types of speakers and events.

### Solution: Intranet WIP guide

A complete guide on the process and value of WIPs will be published on the WIN intranet. This guide will be advertised in Monday messages, so researchers or group leaders new to the process will have ready access to all necessary information. A first draft of the [WIP Guide is available here](WIP-guide.md) (note this guide has been developed for new MRI data collection studies and will need to be adapted/extended for other types of WIPs)


## What are we doing?

The above proposal is being presented to the WIN management board (MB) on 14th July ([slides available here](WIP-Update-slides.pdf)). Discussion will be invited on the suitability of proposed solutions, particularly those listed as open for contribution  below.

Any decisions made and updates to the WIP process will be communicated via WIN Administration.

## How can you contribute?

We would be grateful for your feedback on some of the broader questions in this proposal, particularly those below.

- When should WIPs be triggered?
- For what types of project do WIPs need to be mandatory?
- How can we motivate people to present projects without it becoming a "box-ticking" exercise?
- How can we promote the value of clinical/translational input in methods projects?
- How can we best cultivate an intention of constructive and collaborative audience participation?

## Contact

To discuss or provide feedback on this proposal, please contact cassandra.gouldvanpraag@psych.ox.ac.uk, or submit an [issue on this project](https://git.fmrib.ox.ac.uk/cassag/wip-update/-/issues)

this is me changing something in my file.
